import random
import numpy
import datetime
from deap import base, creator, tools, algorithms

def eval_ind(q):
    n = len(q)
    left_diag = [0] * (2 * n - 1)
    right_diag = [0] * (2 * n - 1)
    for i in range(n):
        left_diag[i+q[i]] += 1
        right_diag[n-i+q[i]-1] += 1
    sum_ = 0
    for i in range(2*n-1):
        if left_diag[i] > 1:
            sum_ += left_diag[i]-1
        if right_diag[i] > 1:
            sum_ += right_diag[i]-1
    return sum_,

NB_QUEENS=200

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()
toolbox.register("permutation", random.sample, range(NB_QUEENS), NB_QUEENS)
# cria função permutation de amostras de 0 a NB_QUEENS-1
# x = toolbox.permutation()
# print(x)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.permutation)
# cria função que cria um individuo permutacao de 0 a NB_QUEENS-1
# i = toolbox.individual()
# print(i)

toolbox.register("population", tools.initRepeat, list, toolbox.individual)
# cria população de tamanho 'n'
# p = toolbox.population(n=10)
# print(p)
# print(len(p))

toolbox.register("evaluate", eval_ind)
# atribui o nome da função 'evaluate' a 'eval_ind'
# i = toolbox.individual()
# print(i)
# print(toolbox.evaluate(i))

toolbox.register("mate", tools.cxPartialyMatched)
# cria função mate (casamento) com algoritmo PMX
# i1 = toolbox.individual()
# i2 = toolbox.individual()
# print(i1)
# print(i2)
# print(toolbox.mate(i1,i2))

toolbox.register("mutate", tools.mutShuffleIndexes, indpb=2.0/NB_QUEENS)
# realiza mutação alterando-se intercâmbio entre genes com probabilidade 1.0/NB_QUEENS
# i = toolbox.individual()
# print(i)
# m = toolbox.mutate(i)
# print(m)

toolbox.register("select", tools.selTournament, tournsize=3)
# realiza seleção por torneio de k individuos, por torneios de tamanho 'tournsize'
# p = toolbox.population(n=6)
# print(p)
# s = toolbox.select(p,k=6)
# print(s)

t1 = datetime.datetime.now()

def main():
    # CXPB  probabilidade de crossover
    # MUTPB probabilidade de mutação
    CXPB, MUTPB = 0.5, 0.2

    # Número de gerações
    NG=1000

    print("Start of evolution")

    # população inicial
    pop = toolbox.population(n=6000)

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in pop if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # configura hall da fama com 1 posição
    hof = tools.HallOfFame(1)
    hof.update(pop)

    g = 1
    # para o algoritmo quando o fitness do melhor individuo for 0
    while eval_ind(hof[0])[0] != 0 and g < NG:

        print("-- Generation %i --" % g)
        # imprime o melhor indíviduo do hall da fama
        print(hof[0], eval_ind(hof[0])[0])

        # seleciona população para cruzamento
        pop_selected = toolbox.select(pop, len(pop))
        offspring = [toolbox.clone(ind) for ind in pop_selected]

        # aplica cruzamento na população selecionada
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < CXPB:
                toolbox.mate(child1, child2)
                # caso ocorra cruzamento, zerar o valor do fitness
                # será recalculado mais a frente
                del child1.fitness.values, child2.fitness.values

        # aplica mutação na população
        for mutant in offspring:
            if random.random() < MUTPB:
                toolbox.mutate(mutant)
                # caso ocorra mutação, zerar o valor do fitness
                # será recalculado mais a frente
                del mutant.fitness.values

        # Recalcular individuos com fitness inválido
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # a população é toda substituída pela geração atual
        pop[:] = offspring
        # atualiza o hall da fama (melhores indivíduos)
        hof.update(pop)

        # gera lista de fitness para cálculos de min, máxim, média e desvio padrão
        fitness_list = [ item[0] for item in toolbox.map(toolbox.evaluate, pop) ]
        length = len(fitness_list)
        mean = sum(fitness_list) / length
        sum2 = sum(x*x for x in fitness_list)
        std = abs(sum2 / length - mean**2)**0.5
        min_ = min(fitness_list)
        max_ = max(fitness_list)
        print(" Min Max  Med          Desvio")
        print("  %2d  %2d %8.5f     %8.5f" % (min_, max_, mean, std))
        g = g + 1

    # imprime o melhor indíviduo do hall da fama
    print(hof[0], eval_ind(hof[0])[0])
    t2 = datetime.datetime.now()
    print("tempo gasto: "+str(t2-t1))

if __name__ == "__main__":
    main()